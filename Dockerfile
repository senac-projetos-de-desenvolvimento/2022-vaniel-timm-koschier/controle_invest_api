FROM node:latest

RUN mkdir -p /home/app/
WORKDIR /home/app/

COPY package.json package-lock.json ./

RUN npm i 
COPY . .
EXPOSE 3001


RUN ls
CMD ["npm", "start"]
