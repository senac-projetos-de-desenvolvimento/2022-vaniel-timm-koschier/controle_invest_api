# Controle Invest API
## Baixar o projeto na sua maquina
```sh
git clone https://gitlab.com/senac-projetos-de-desenvolvimento/2022-vaniel-timm-koschier/controle_invest_api.git
```
## Baixar as dependencias 
### Entrar na pasta do projeto controle_invest_api e rodar o seguinte comando:
```sh
npm i
```
## Rodar o projeto 
### Dentro da pasta controle_invest_api acessar a pasta src e rodar o seguinte comando:
```sh
node server.js
```
