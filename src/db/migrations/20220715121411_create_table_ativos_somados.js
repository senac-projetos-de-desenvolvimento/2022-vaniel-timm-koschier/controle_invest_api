exports.up = function(knex) {
    return knex.schema.createTable('ativos_somados', (table) => {
        table.increments('id').primary();
        table.string('nome', 80).notNullable()
        table.string('codigo').notNullable()
        table.string('setor', 80).notNullable()
        table.float('valor_medio').notNullable()
        table.float('total_ativos').notNullable()
        table.float('valor_total').notNullable()
        table.timestamps(true, true)
      })
};

exports.down = (knex) => knex.schema.dropTable('ativos_somados')
