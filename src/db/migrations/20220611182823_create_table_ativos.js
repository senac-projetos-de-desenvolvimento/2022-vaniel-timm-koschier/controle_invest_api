exports.up = function(knex) {
    return knex.schema.createTable('ativos', (table) => {
        table.increments('');
        table.string('ordem').notNullable()
        table.string('nome', 80).notNullable()
        table.string('codigo').notNullable()
        table.string('setor', 80).notNullable()
        table.float('valor').notNullable()
        table.float('valor_real').notNullable()
        table.float('quantidade').notNullable()
        table.string('data_op', 60).notNullable()

        table.timestamps(true, true)
      })
};

exports.down = (knex) => knex.schema.dropTable('ativos')


// exports.up = function(knex) {
//     return knex.schema.createTable('ativos', (table) => {
//         table.increments('');
//         table.string('nome', 80).notNullable()
//         table.string('codigo').notNullable()
//         table.decimal('valor',9.2).notNullable()
//         table.boolean('quantidade').notNullable()
//         table.string('data_op', 60).notNullable()
        
//         table.integer('ordem_id').notNullable().unsigned()
//         table.foreign('ordem_id')
//             .references('tipo_ordem.id')
//             .onDelete('restrict')
//             .onUpdate('cascade')

//         table.integer('setor_id').notNullable().unsigned()
//         table.foreign('setor_id')
//             .references('setores.id')
//             .onDelete('restrict')
//             .onUpdate('cascade')
        
//         table.timestamps(true, true)
//       })
// };

// exports.down = (knex) => knex.schema.dropTable('ativos')
