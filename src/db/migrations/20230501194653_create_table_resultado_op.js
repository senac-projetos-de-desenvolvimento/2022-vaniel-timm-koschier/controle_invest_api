exports.up = function(knex) {
  return knex.schema.createTable('resultado_op', (table) => {
      table.increments('id').primary();
      table.string('nome', 80).notNullable();
      table.string('codigo').notNullable();
      table.string('setor', 80).notNullable();
      table.float('resultado').notNullable();
      table.float('diferenca_percentual').notNullable();
      table.date('data_op').notNullable();
      table.timestamps(true, true);
  });
};

exports.down = (knex) => knex.schema.dropTable('resultado_op');
