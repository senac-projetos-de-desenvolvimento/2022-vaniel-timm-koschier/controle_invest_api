exports.up = (knex) => {
    return knex.schema.createTable('dados_usuarios', (table) => {
        table.increments('id').primary();
        table.string('cpf', 60).notNullable()
        table.string('nome', 60).notNullable()
        table.string('data_nasc', 60).notNullable()
        table.string('email', 120).unique().notNullable()
        table.timestamps(true, true)
      })
};

exports.down = (knex) => {
    
    knex.schema.dropTable('dados_usuarios')
}