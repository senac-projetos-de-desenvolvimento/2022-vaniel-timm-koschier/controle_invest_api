exports.seed = function(knex) {
  return knex('setores').del()
    .then(function () {
      return knex('setores').insert([
        {setor:'Utilities'},
        {setor:'Financeiro'},
        {setor:'Técnologia'},
        {setor:'Metais e Mineração'},
        {setor:'Infraestrutura'},
        {setor:'Saúde'},
        {setor:'Agro'},
        {setor:'Alimentos e Bebidas'},
        {setor:'Varejo'},
        {setor:'Aluguel de Veículos'},
      ]);
    });
};
