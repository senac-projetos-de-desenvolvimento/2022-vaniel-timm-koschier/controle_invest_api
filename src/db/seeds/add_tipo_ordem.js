exports.seed = function(knex) {
  return knex('tipo_ordem').del()
    .then(function () {
      return knex('tipo_ordem').insert([
        {ordem:'Ordem de Compra'},
        {ordem:'Ordem de Venda'},
      ]);
    });
};