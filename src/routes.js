const express = require('express');
const routes = express.Router();
const cors = require('cors');
require('dotenv').config();

routes.use(cors());

// const authMiddleware = require('./controllers/authMiddleware');

const AtivoController = require('./controllers/AtivoController');
const OrdemController = require('./controllers/OrdemController');
const SetorController = require('./controllers/SetorController');
const Ativos_SomadosController = require('./controllers/Ativos_SomadosController');
const Dados_Users_Controller = require('./controllers/Dados_UsersController');
const ResultadoController = require('./controllers/ResultadoController');

routes.post('/usuarios', Dados_Users_Controller.store);
routes.post('/login', Dados_Users_Controller.login);
routes.get('/usuarios', Dados_Users_Controller.index);
routes.delete('/logout', Dados_Users_Controller.logout);

// routes.use(authMiddleware);

routes.get('/ativos', AtivoController.index);
routes.post('/adiciona', AtivoController.store);
routes.put('/edita/:id', AtivoController.update);
routes.get('/ativo/:id', AtivoController.getAtivo);
routes.delete('/exclui/:id', AtivoController.delete);
routes.delete('/exclui_somados/:id', AtivoController.deleteAtivosSomados)
routes.delete('/exclui_resultado/:id', AtivoController.deleteResultadoOp)

routes.get('/ativos_somados', Ativos_SomadosController.index);
routes.get('/porcent_ativos', Ativos_SomadosController.porcetagem_ativos);
routes.get('/porcent_setor', Ativos_SomadosController.porcentagem_setor);

routes.get('/resultado_op', ResultadoController.index);

routes.get('/ordem', OrdemController.ordem);

routes.get('/setores', SetorController.setor);

module.exports = routes;
