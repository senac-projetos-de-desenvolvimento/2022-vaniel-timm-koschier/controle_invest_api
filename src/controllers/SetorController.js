
const knex = require("../db/dbconfig")


module.exports = {

    async setor(req, res) {
        const setores = await knex("setores")
        res.status(200).json(setores)
    }
}