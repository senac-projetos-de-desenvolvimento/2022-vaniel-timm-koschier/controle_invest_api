const knex = require("../db/dbconfig")

module.exports = {
    async index(req, res) {
        try {
          let sql = "SELECT * FROM ativos_somados atisom "
          sql += "WHERE valor_medio != 0 AND total_ativos != 0 "
          sql += "GROUP BY id "
          sql += "HAVING id = (SELECT MAX(id) FROM ativos_somados WHERE ativos_somados.nome = atisom.nome) "
          sql += "ORDER BY nome; "
          const resp = await knex.raw(sql)
          const ativos_somados = resp[0];
          return res.status(200).json(ativos_somados);
        } catch (error) {
          return res.status(400).json({ erro: error.message });
        }
      },
      async porcetagem_ativos(req, res) {
        try {
          // Primeiro, obtenha todos os ativos somados
          const ativos_somados = await knex('ativos_somados').select('nome', 'valor_medio', 'total_ativos');
      
          // Em seguida, calcule o valor_total para cada ativo somado usando valor_medio * total_ativos
          ativos_somados.forEach((ativo) => {
            ativo.valor_total = ativo.valor_medio * ativo.total_ativos;
          });
      
          // Calcule o total somado usando a soma de todos os valor_total
          const total = ativos_somados.reduce((sum, ativo) => sum + ativo.valor_total, 0);
      
          // Calcule a porcentagem para cada ativo somado
          ativos_somados.forEach((ativo) => {
            ativo.percentage = (ativo.valor_total / total) * 100;
          });
      
          return res.status(200).json(ativos_somados);
        } catch (error) {
          return res.status(400).json({ erro: error.message });
        }
      },

      async porcentagem_setor(req, res) {
        try {
          // Primeiro, obtenha todos os ativos somados
          const ativos_somados = await knex('ativos_somados').select('setor', 'valor_medio', 'total_ativos');
      
          // Em seguida, calcule o valor_total para cada ativo somado usando valor_medio * total_ativos
          ativos_somados.forEach((ativo) => {
            ativo.valor_total = ativo.valor_medio * ativo.total_ativos;
          });
      
          // Agrupe os ativos somados pelo setor e some os valor_total para cada setor
          const setores = {};
          ativos_somados.forEach((ativo) => {
            if (!setores[ativo.setor]) {
              setores[ativo.setor] = 0;
            }
            setores[ativo.setor] += ativo.valor_total;
          });
      
          // Calcule o total somado usando a soma de todos os valor_total dos setores
          const total = Object.values(setores).reduce((sum, valor_total) => sum + valor_total, 0);
      
          // Calcule a porcentagem para cada setor
          const porcentagem_setor = Object.entries(setores).map(([setor, valor_total]) => ({
            setor,
            percentage: (valor_total / total) * 100,
          }));
      
          return res.status(200).json(porcentagem_setor);
        } catch (error) {
          return res.status(400).json({ erro: error.message });
        }
      }
      
}