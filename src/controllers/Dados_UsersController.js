const knex = require("../db/dbconfig");
require("dotenv").config();
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

let transporter = nodemailer.createTransport({
  service: 'hotmail',
  auth: {
    user: 'seu-email@hotmail.com',
    pass: 'sua-senha'
  }
});

module.exports = {

  async index(req, res) {
    const usuarios = await knex("usuarios");
    res.status(200).json(usuarios);
  },

  async store(req, res) {

    const { cpf, data_nasc, nome, email } = req.body;
    if (!nome || !email || !cpf || !data_nasc) {
      res.status(400).json({ erro: "Favor Enviar nome, email, cpf e Data de Nascimento " });
      return;
    }
    try {
      const dados = await knex("usuarios").where({ email });
      if (dados.length) {
        res.status(400).json({ erro: "Email já Cadastrado" });
        return;
      }
    } catch (error) {
      res.status(400).json({ erro: error.message });
    }
    const senha = crypto.randomBytes(4).toString('hex');
    const hash = bcrypt.hashSync(senha, 10);
    console.log(senha);
    let mailOptions = {
      from: 'vanieltk@hotmail.com',
      to: email,
      subject: 'paterno',
      text: 'Sua senha é: ' + senha
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email enviado: ' + info.response);
      }
    });

    try {
      const novo = await knex("usuarios").insert({ cpf, data_nasc, nome, email, senha: hash});
      res.status(201).json({ id: novo[0] });
    } catch (error) {
      res.status(400).json({ erro: error.message });
    }
  },

  async login(req, res) {
    const { email, senha } = req.body;
    console.log("Login request:", email, senha);
  
    if (!email || !senha) {
      res.status(400).json({ erro: "Login ou senha Incorretos" });
      return;
    }
  
    try {
      const dados = await knex("usuarios").where({ email });
  
      if (dados.length == 0) {
        console.log("Invalid login attempt for email:", email);
        res.status(400).json({ erro: "Login ou Senha Incorretos" });
        return;
      }
  
      if (bcrypt.compareSync(senha, dados[0].senha)) {
        const token = jwt.sign(
          {
            usuario_id: dados[0].id,
            usuario_nome: dados[0].nome,
          },
          "jdnsjdfjdfjdbfjdnfkdjfldjfkdfjdkf",
          {
            expiresIn: "1h",
          }
        );
        console.log("Token generated:", token);
        res.status(200).json({ id: dados[0].id, nome: dados[0].nome, token });
      } else {
        console.log("Invalid login attempt for email:", email);
        res.status(400).json({ erro: "Login ou Senha Incorretos" });
      }
    } catch (error) {
      console.log("Error occurred during login:", error);
      res.status(400).json({ error });
    }
  },

  async logout(req, res) {
    const token = req.headers.authorization.split(' ')[1];
    res.status(200).json({ message: 'Logout realizado com sucesso' });
  }
};