const knex = require("../db/dbconfig")
const { connection } = require("../knexfile")

module.exports = {
    async index(req, res) {
        const resultado_op = await knex("resultado_op")
        res.status(200).json(resultado_op)
    },
}