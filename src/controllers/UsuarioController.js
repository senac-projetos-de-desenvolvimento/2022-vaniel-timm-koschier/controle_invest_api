const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const knex = require("../db/dbconfig")
const nodemailer = require("nodemailer")
const crypto = require("crypto");
require("dotenv").config();

let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "vanielpdc@gmail.com",
    pass: "residencia",
  },
});

module.exports = {

  async index(req, res) {
    const usuarios = await knex("usuarios")
    res.status(200).json(usuarios)
  },

  async store(req, res) {

    const { cpf, data_nasc,nome, email } = req.body
    if (!nome || !email || !cpf || !data_nasc) {
      res.status(400).json({ erro: "Favor Enviar nome e email " })
      return
    }
    try {
      const dados = await knex("usuarios").where({ email });
      if (dados.length) {
        res.status(400).json({ erro: "Email já Cadastrado" })
        return
      }
    } catch (error) {
      res.status(400).json({ erro: error.message })
    }

    // Gera uma senha aleatória
    const senha = crypto.randomBytes(4).toString('hex');
    const hash = bcrypt.hashSync(senha, 10);

    try {
      const novo = await knex("usuarios").insert({ cpf, data_nasc, nome, email, senha: hash })

      // Aqui estamos enviando o email com a senha
      await transporter.sendMail({
        from: '"Your App" <vanielpdc@gmail.com>', // sender address
        to: email, // receiver is the user's email
        subject: "Bem-vindo à nossa aplicação", // Subject line
        text: `Olá, ${nome}! Bem-vindo à nossa aplicação. Sua senha é ${senha}.`, // plain text body
        html: `<b>Olá, ${nome}! Bem-vindo à nossa aplicação. Sua senha é ${senha}.</b>`, // html body
      });

      res.status(201).json({ id: novo[0] })

    } catch (error) {
      res.status(400).json({ erro: error.message })
    }
  },

  async login(req, res) {
    const { email, senha } = req.body
    if (!email || !senha) {
      res.status(400).json({ erro: "Login ou senha Incorretos" })
      return;
    }

    try {

      const dados = await knex("usuarios").where({ email })

      if (dados.length == 0) {
        res.status(400).json({ erro: "Login ou Senha Incorretos" })
        return
      }

      if (bcrypt.compareSync(senha, dados[0].senha)) {
        const token = jwt.sign({
          usuario_id: dados[0].id,
          usuario_nome: dados[0].nome
        }, "" + process.env.JWT_KEY,
          {
            expiresIn: "1h"
          }
        )
        res.status(200).json({id: dados[0].id, nome: dados[0].nome, token})


      } else {
        res.status(400).json({ erro: "Login ou Senha Incorretos" })
      }

    } catch (error) {
      res.status(400).json({error})
      console.log(error)
    }
  },

  async logout(req, res) {
    const token = req.headers.authorization.split(' ')[1];
    res.status(200).json({ message: 'Logout realizado com sucesso' });
  }
}
