const knex = require("../db/dbconfig")
const { connection } = require("../knexfile")


module.exports = {
    async index(req, res) {
        const ativos = await knex("ativos")
        res.status(200).json(ativos)
    },

    async store(req, res) {
        const { nome, ordem, codigo, setor, valor, quant, data_op } = req.body

        if (!nome || !ordem || !codigo || !setor || !valor || !quant || !data_op) {
           return res.status(400).json({ erro: "Favor Enviar Todas as Informações" })
        }
        const formattedDate = new Date(data_op).toLocaleDateString("pt-BR");

        try {
            if (ordem === "Ordem de Compra") {
                const valor_real = (valor) * (quant)
                const quantidade = quant
                const novo = await knex("ativos").insert({ nome, ordem, codigo, setor, valor, valor_real, quantidade, data_op:formattedDate })
                res.status(201).json({ id: novo[0] })
            } if (ordem === "Ordem de Venda") {
                const valor_real = ((valor) * -(quant))
                const quantidade = quant * -1
                // console.log(valor)
                // console.log(quantidade)
                // console.log(valor_real)
                const novo = await knex("ativos").insert({ nome, ordem, codigo, setor, valor, valor_real, quantidade, data_op:formattedDate })
                res.status(201).json({ id: novo[0] })
            }
        } catch (error) {
           return res.status(400).json({ erro: error.message })
        }

        try {
            const ativos_ordems = await knex("ativos").where({ codigo });
            if (codigo.length) {
                const updated_at = {};
                const data_op = {};
                const valor_somado = {};
                const total_ativos = {};
                const nome = {};
                const setor = {};
                const valor = {};
                const valor_medio = {};
                const resultado = {};
                const resultadosInseridos = [];
        
                for (const elem of ativos_ordems) {
                    data_op[elem.codigo] = elem.data_op;
                    nome[elem.codigo] = elem.nome;
                    setor[elem.codigo] = elem.setor;
                    valor[elem.codigo] = elem.valor;
                    const valorAnterior = valor_somado[elem.codigo] || 0;
                    const total_anterior = total_ativos[elem.codigo] || 0;
                    total_ativos[elem.codigo] = total_anterior + elem.quantidade;
                    valor_somado[elem.codigo] = valorAnterior + elem.valor_real;
                
                    if (total_ativos[elem.codigo] > total_anterior) {
                        valor_medio[elem.codigo] = valor_somado[elem.codigo] / total_ativos[elem.codigo];
                        console.log(total_ativos);
                        console.log(valor_somado);
                        console.log(valor_medio);
                    } else if (total_ativos[elem.codigo] < total_anterior) {
                        const quantidadeVendida = total_anterior - total_ativos[elem.codigo];
                        const valorVendido = valor[elem.codigo] * quantidadeVendida;
                        const valor_equivalente_venda = valor_medio[elem.codigo] * quantidadeVendida;
                        valor_somado[elem.codigo] = valorAnterior - valor_equivalente_venda;
                
                        if (valor_equivalente_venda !== 0) {
                            diferenca_percentual = ((valorVendido - valor_equivalente_venda) / valor_equivalente_venda) * 100;
                        }
        
                        if (total_ativos[elem.codigo] > 1) {
                            resultado[elem.codigo] = valorVendido - valor_equivalente_venda;
                            console.log("Resultado");
                            console.log(resultado);
                        }
                        if (total_ativos[elem.codigo] <= 0) {
                            resultado[elem.codigo] = valorVendido - valor_equivalente_venda;
                            console.log("Resultado");
                            console.log(resultado);
                            valor_medio[elem.codigo] = 0;
                            valor_somado[elem.codigo] = 0;
                            console.log(total_ativos);
                            console.log(valor_somado);
                            console.log(valor_medio);
                            nome[elem.codigo] = elem.nome;
                            setor[elem.codigo] = elem.setor;
                        }
        
                        const resultado_op = {
                            nome: nome[elem.codigo],
                            codigo: elem.codigo,
                            setor: setor[elem.codigo],
                            resultado: parseFloat(resultado[elem.codigo]).toFixed(2),
                            diferenca_percentual: diferenca_percentual.toFixed(2),
                            data_op: data_op[elem.codigo],
                        };
                
                        const ultima_ordem_venda = await knex('resultado_op')
                        .where('codigo', elem.codigo)
                        .orderBy('updated_at', 'desc')
                        .first();
                    
                        // Verifica se a operação atual é uma nova operação de venda
                         if (!ultima_ordem_venda || ultima_ordem_venda.updated_at < elem.updated_at) {
                        // Insere um novo registro
                        const novo_resultado = await knex('resultado_op').insert(resultado_op);
                        resultadosInseridos.push(novo_resultado[0]);
                        } else if (!ultima_ordem_venda) {
                        // Se não houver operação de venda registrada ainda, insere um novo registro
                        const novo_resultado = await knex('resultado_op').insert(resultado_op);
                        resultadosInseridos.push(novo_resultado[0]);
                    }
                    
                    }
                }
                
                const ativos_somados = Object.keys(valor_medio).map((key) => {
                    const valorTotal = parseFloat(valor_medio[key]) * parseFloat(total_ativos[key]);
                    return {
                        nome: nome[key],
                        codigo: key,
                        setor: setor[key],
                        valor_medio: parseFloat(valor_medio[key]).toFixed(2),
                        total_ativos: parseFloat(total_ativos[key]).toFixed(2),
                        valor_total: valorTotal.toFixed(2),
                    };
                });
        
                for (const ativo of ativos_somados) {
                    const existingAtivo = await knex('ativos_somados').where('codigo', ativo.codigo).first();
                    
                    if (existingAtivo) {
                      await knex('ativos_somados').where('codigo', ativo.codigo).update(ativo);
                    } else {
                      await knex('ativos_somados').insert(ativo);
                    }
                  }
        
            }
        } catch (error) {
            // Handle your error here...
        }
        

},

async getAtivo(req, res) {
    const id = req.params.id;
    try {
      const ordem = await knex('ativos').where({ id }).first();
      if (!ordem) {
        return res.status(404).json({ erro: 'Ordem não encontrada' });
      }
      res.status(200).json(ordem);
    } catch (error) {
      res.status(400).json({ erro: error.message });
    }
  },

  async update(req, res) {
    const id = req.params.id;
    const { nome, ordem, codigo, setor, valor, quantidade, data_op } = req.body;
  
    try {
      const ordemAnterior = await knex("ativos").where({ id }).first();
      // Obtemos a ordem anterior do banco de dados
  
      if (ordemAnterior.ordem === "Ordem de Venda" && (ordem !== ordemAnterior.ordem || valor !== ordemAnterior.valor || quantidade !== ordemAnterior.quantidade)) {
        return res.status(400).send("Não é possível modificar o tipo de ordem, valor ou quantidade em uma ordem de venda.");
      }
  
      const valor_real = ordem === "Ordem de Compra" ? valor * quantidade : -valor * quantidade;
      // Calculamos o novo valor_real com base no tipo de ordem
  
      await knex("ativos")
        .where({ id })
        .update({ nome, ordem, codigo, setor, valor, quantidade, valor_real, data_op });
      // Atualizamos a ordem no banco de dados com os novos dados fornecidos
  
      const ativos_ordems = await knex("ativos").where({ codigo });
      // Buscamos todas as ordens de compra ou venda com o mesmo código
  
      const valor_somado = ativos_ordems.reduce((total, ordem) => {
        return total + ordem.valor_real;
      }, 0);
      // Calculamos o valor somado das ordens encontradas
  
      const total_ativos = ativos_ordems.reduce((total, ordem) => {
        return total + ordem.quantidade;
      }, 0);
      // Calculamos o total de ativos das ordens encontradas
  
      const valor_medio = total_ativos === 0 ? 0 : valor_somado / total_ativos;
      // Calculamos o valor médio dos ativos
  
      await knex("ativos_somados")
        .where({ codigo })
        .update({ valor_medio, total_ativos });
      // Atualizamos a tabela 'ativos_somados' com os novos valores médio e total de ativos
  
      if (ordemAnterior.ordem !== ordem) {
        // Verificamos se houve uma alteração no tipo de ordem
  
        const resultadoOp = await knex("resultado_op")
          .where({ codigo })
          .orderBy("updated_at", "desc")
          .first();
        // Buscamos o último resultado registrado na tabela 'resultado_op' para o código especificado
  
        if (resultadoOp) {
          const valorTotal = valor_medio * total_ativos;
          // Calculamos o novo valor total com base no novo valor médio e total de ativos
  
          const resultado = valorTotal - resultadoOp.valor_total;
          // Calculamos o novo resultado subtraindo o valor total atual do resultado anterior
  
          await knex("resultado_op")
            .where({ id: resultadoOp.id })
            .update({ resultado });
          // Atualizamos o resultado na tabela 'resultado_op'
  
          const ativoSomado = await knex("ativos_somados")
            .where({ codigo })
            .first();
          // Buscamos o ativo somado na tabela 'ativos_somados'
  
          if (ativoSomado) {
            const valorTotalAtualizado = parseFloat(ativoSomado.valor_total) + resultado;
            // Calculamos o novo valor total atualizado adicionando o resultado
  
            await knex("ativos_somados")
              .where({ codigo })
              .update({ valor_total: valorTotalAtualizado });
            // Atualizamos o valor total na tabela 'ativos_somados'
          } else {
            const novoAtivoSomado = {
              codigo,
              valor_medio,
              total_ativos,
              valor_total: resultado,
            };
  
            await knex("ativos_somados").insert(novoAtivoSomado);
            // Criamos um novo registro na tabela 'ativos_somados' com os valores atualizados
          }
        }
      }
  
      res.sendStatus(200);
    } catch (error) {
      console.error(error);
      res.sendStatus(500);
    }
  },
  
  

  async delete(req, res) {
    const id = req.params.id;
    try {
      const ordemExcluida = await knex("ativos").select("*").where({ id }).first();
      await knex("ativos").del().where({ id });
  
      // Excluir registro da tabela resultado_op se for uma ordem de venda
      if (ordemExcluida.ordem === "Ordem de Venda") {
        await knex("resultado_op")
          .where({ codigo: ordemExcluida.codigo, updated_at: ordemExcluida.updated_at })
          .del();
      }
  
      // Recalcular valores somados e atualizar tabela ativos_somados
      const ativos_ordems = await knex("ativos").where({ codigo: ordemExcluida.codigo });
  
      const valor_somado = ativos_ordems.reduce((total, ordem) => {
        return total + ordem.valor_real;
      }, 0);
  
      const total_ativos = ativos_ordems.reduce((total, ordem) => {
        return total + ordem.quantidade;
      }, 0);
  
      
      await knex("ativos_somados")
        .where({ codigo: ordemExcluida.codigo })
        .update({total_ativos });
  
      // Recalculate valor_total for remaining orders
      const remainingOrders = await knex("ativos").where({ codigo: ordemExcluida.codigo });
  
      let valor_total = 0;
      for (const order of remainingOrders) {
        valor_total += parseFloat(order.valor_real);
      }
  
      await knex("ativos_somados")
        .where({ codigo: ordemExcluida.codigo })
        .update({ valor_total });
  
      res.status(200).json("Ordem excluída");
    } catch (error) {
      res.status(400).json({ msg: error.message });
    }
  },
  
  async porcetagem_ativos(req, res) {
    try {
      // Primeiro, obtenha todos os ativos somados
      const ativos_somados = await knex('ativos_somados').select('nome', 'valor_medio', 'total_ativos');
  
      // Em seguida, calcule o valor_total para cada ativo somado usando valor_medio * total_ativos
      ativos_somados.forEach((ativo) => {
        ativo.valor_total = ativo.valor_medio * ativo.total_ativos;
      });
  
      // Calcule o total somado usando a soma de todos os valor_total
      const total = ativos_somados.reduce((sum, ativo) => sum + ativo.valor_total, 0);
  
      // Calcule a porcentagem para cada ativo somado
      ativos_somados.forEach((ativo) => {
        ativo.percentage = (ativo.valor_total / total) * 100;
      });
  
      return res.status(200).json(ativos_somados);
    } catch (error) {
      return res.status(400).json({ erro: error.message });
    }
  },

    async porcentagem_setor(req, res) {
      try {
        // Primeiro, obtenha todos os ativos somados
        const ativos_somados = await knex('ativos_somados').select('setor', 'valor_medio', 'total_ativos');
    
        // Em seguida, calcule o valor_total para cada ativo somado usando valor_medio * total_ativos
        ativos_somados.forEach((ativo) => {
          ativo.valor_total = ativo.valor_medio * ativo.total_ativos;
        });
    
        // Agrupe os ativos somados pelo setor e some os valor_total para cada setor
        const setores = {};
        ativos_somados.forEach((ativo) => {
          if (!setores[ativo.setor]) {
            setores[ativo.setor] = 0;
          }
          setores[ativo.setor] += ativo.valor_total;
        });
    
        // Calcule o total somado usando a soma de todos os valor_total dos setores
        const total = Object.values(setores).reduce((sum, valor_total) => sum + valor_total, 0);
    
        // Calcule a porcentagem para cada setor
        const porcentagem_setor = Object.entries(setores).map(([setor, valor_total]) => ({
          setor,
          percentage: (valor_total / total) * 100,
        }));
    
        return res.status(200).json(porcentagem_setor);
      } catch (error) {
        return res.status(400).json({ erro: error.message });
      }
    
  },
  async deleteResultadoOp(req, res) {
    const id = req.params.id; // Obtém o ID do registro a ser deletado
  
    try {
      const deletedRows = await knex('resultado_op').where('id', id).del();
      
      if (deletedRows > 0) {
        res.status(200).json({ mensagem: 'Registro excluído com sucesso na tabela resultado_op.' });
      } else {
        res.status(404).json({ mensagem: 'Registro não encontrado na tabela resultado_op.' });
      }
    } catch (error) {
      res.status(500).json({ erro: error.message });
    }
  },
async deleteAtivosSomados(req, res) {
const id = req.params.id; // Obtém o ID do registro a ser deletado

try {
  const deletedRows = await knex('ativos_somados').where('id', id).del();
  
  if (deletedRows > 0) {
    res.status(200).json({ mensagem: 'Registro excluído com sucesso na tabela ativos_somados.' });
  } else {
    res.status(404).json({ mensagem: 'Registro não encontrado na tabela ativos_somados.' });
  }
} catch (error) {
  res.status(500).json({ erro: error.message });
}
} 
  
}
