const express = require('express')
const app = express()
const routes = require('./routes')
const database = require('./db/dbconfig')
require("dotenv").config();

app.get('/', (req, res) => {
  res.send('Hello World!')
})
app.use(express.json())
app.database = database

app.use(routes)

module.exports = app