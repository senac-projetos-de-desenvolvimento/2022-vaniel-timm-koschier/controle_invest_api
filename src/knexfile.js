require('dotenv').config()

module.exports = {
  client: 'mysql2',
  connection: {
    host : "54.232.43.95",
    user : "userinvest",
    password: "kRUA7%FmW",
    database : "controleinvest-prod",
    port : "3306"
  },
  pool: {
    min: 0,
    max: 7
  },
  migrations: {
    tablenName:'knex_migrations',
    directory:'./db/migrations'
  },
  seeds: {
    directory: './db/seeds'
  }
}
